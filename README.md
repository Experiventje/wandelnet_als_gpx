# Wandelnet als gpx

Convert wandelnet routes naar een gpx bestand voor het gebruik in JOSM, om zo het wandelnetwerk sneller in te kunnen vullen voor OpenStreetMap.

Als bron gebruik ik de website [wandelnet.nl](https://wandelnet.planner.routemaker.nl/planner/wandelen).

Het JSON bestand is te downloaden met Shift + F5 in Firefox, inzoomen op de wenselijke regio, en dan onder Netwerk te dubbelklikken op bestand getNetwork, type json.
![Screenshot json bestand downloaden](https://i.ibb.co/PY1Mjbk/Screenshot-20201014-160637.png)
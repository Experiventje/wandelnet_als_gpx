from pyproj import Transformer
import json

json_file = 'getNetwork.json'
nodes_gpx = 'nodes.gpx'
routes_gpx = 'routes.gpx'

node_network = json.load(open(json_file))

transformer = Transformer.from_crs("epsg:3857", "epsg:4326")

gpx_file = open(nodes_gpx, 'w')
gpx_file.write('<?xml version="1.0" encoding="UTF-8" standalone="no" ?>\n<gpx creator="Python3" version="1.1">\n')
gpx_file.close()

for i in node_network["result"]["pois"]:
    if i["name"] and i["geom"]:
        lat, lon = transformer.transform(i["geom"][0], i["geom"][1])
        gpx_file = open(nodes_gpx, 'a')
        gpx_file.write('<wpt lat="{}" lon="{}"><name>{}</name></wpt>\n'.format(lat, lon, i["name"]))
        gpx_file.close()

gpx_file = open(nodes_gpx, 'a')
gpx_file.write('</gpx>')
gpx_file.close()

gpx_file = open(routes_gpx, 'w')
gpx_file.write('<?xml version="1.0" encoding="UTF-8" standalone="no" ?>\n<gpx creator="Python3" version="1.1">\n')
gpx_file.close()

nodes = {}
for i in node_network["result"]["lines"]:
    if i["geom"]:
        gpx_file = open(routes_gpx, 'a')
        gpx_file.write('<trk><trkseg>')
        for j in i["geom"]:
            lat, lon = transformer.transform(j[0], j[1])
            gpx_file.write('<trkpt lat="{}" lon="{}" />'.format(lat, lon))
        gpx_file.write('</trkseg></trk>\n')
        gpx_file.close()
        
gpx_file = open(routes_gpx, 'a')
gpx_file.write('</gpx>')
gpx_file.close()
